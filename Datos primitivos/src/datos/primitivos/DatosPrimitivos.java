/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos.primitivos;
import java.net.*;
import java.io.*;
/**
 *
 * @author esierray1500
 */
public class DatosPrimitivos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try{
            DatagramSocket s = new DatagramSocket(2000);
            System.out.println("Servidor iniciado");
            for(;;){
                DatagramPacket p = new DatagramPacket(new byte[2000],2000);
                s.receive(p);
                System.out.println("Dataagrama recibido desde: "+p.getAddress()+":"+p.getPort());
                DataInputStream dis = new DataInputStream(new ByteArrayInputStream(p.getData()));
                int x = dis.readInt();
                float f = dis.readFloat();
                long l = dis.readLong();
                System.out.println("\n\nEntero: "+ x + "\nFlotante: "+ f + "\nLargo: "+l);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        // TODO code application logic here
    }
    
}
