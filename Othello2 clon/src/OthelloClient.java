/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.net.*;
import java.io.*;
import java.util.*;

/**
 *
 * @author Eduardo
 */
public class OthelloClient {

    public static void main(String[] args) {
        int jugador;//1= jugador 1//2 = jugador 2
        try {
            BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Escribe la direccion del servidor");
            String host = br1.readLine();
            System.out.println("Escriba el puerto ");
            int pto = Integer.parseInt(br1.readLine());
            //creamos el socket y nos conectamos
            Socket cl = new Socket(host, pto);
            BufferedReader br2 = new BufferedReader(new InputStreamReader(cl.getInputStream()));
            //nos conectamos
            String mensaje = br2.readLine();
            System.out.println("Mensaje: " + mensaje);
            br1.close();
            br2.close();
            cl.close();
            String[] msj2 = mensaje.split("-");
            if ("Conectandose al juego".equals(msj2[0])) {
                cl = new Socket(host, Integer.parseInt(msj2[1]));
                br2 = new BufferedReader(new InputStreamReader(cl.getInputStream()));
                //nos conectamos
                mensaje = br2.readLine();
                System.out.println("Mensaje: " + mensaje);
                msj2 = mensaje.split("-");
                jugador = Integer.parseInt(msj2[1]);
                Othello app = new Othello(true);//se crea la partida con turno inicio jugador 1
                if (jugador == 1) {
                    app.getGPanel().setJugador(true);
                } else {
                    app.getGPanel().setJugador(false);
                }
                PrintWriter pw = new PrintWriter(new OutputStreamWriter(cl.getOutputStream()));
                Move mov = null;
                while (true) {//duracion del juego
                    if (jugador == 1) {
                        if (app.getGPanel().getUltimo() != null) {//checa si presiono una casilla y la envia
                            mensaje = "" + app.getGPanel().getUltimo().getI() + "-" + app.getGPanel().getUltimo().getJ();
                            app.getGPanel().rUltimo();
                            pw.flush();
                            pw.println(mensaje);//envia coordenada a sevidor j1
                            pw.flush();
                            System.out.println("Se envio jugada al j2 en espera de recibir respuesta");
                            mensaje = br2.readLine(); //recive coordenada de j2
                            System.out.println("Se recibio respuesta");
                            String[] coordenada = mensaje.split("-");
                            mov = new Move(Integer.parseInt(coordenada[0]), Integer.parseInt(coordenada[1]));
                            System.out.println("Mensaje: " + mensaje);
                            app.getGPanel().mouseImitador(mov);//realiza el movimiento del enemigo
                        }
                    } else {//jugador ==2
                        mensaje = br2.readLine(); //recive coordenada de j1
                        String[] coordenada = mensaje.split("-");
                        mov = new Move(Integer.parseInt(coordenada[0]), Integer.parseInt(coordenada[1]));
                        System.out.println("Mensaje: " + mensaje);
                        app.getGPanel().mouseImitador(mov);//realiza el movimiento del enemigo
                        while (true) {
                            if (app.getGPanel().getUltimo() != null) {//checa si presiono una casilla y la envia
                                mensaje = "" + app.getGPanel().getUltimo().getI() + "-" + app.getGPanel().getUltimo().getJ();
                                mov = new Move(app.getGPanel().getUltimo().getI(), app.getGPanel().getUltimo().getJ());
                                app.getGPanel().mouseImitador(mov);//realiza su propio movimiento
                                app.getGPanel().rUltimo();
                                pw.flush();
                                pw.println(mensaje);//envia coordenada a sevidor j2
                                pw.flush();
                                break;
                            }
                            Thread.sleep(500);
                        }
                    }
                    Thread.sleep(500);
                }
                br1.close();
                br2.close();
                pw.close();
                cl.close();
            }
        }//try
        catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error");
        }//try catch
    }//main
}
