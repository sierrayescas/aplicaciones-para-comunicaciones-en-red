
import java.io.IOException;
import java.nio.channels.*;
import java.net.*;
import java.io.*;
import java.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Eduardo
 */
//primer hilo acetar conexiones
//4 siguientes hilos partidas
// hilo siguiente servidor
public class OthelloServer extends Thread {

    public static void main(String[] args) {
        // TODO code application logic here
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        Thread c1 = new Thread(new Conexiones(num));
        Thread c2 = new Thread(new partidaC(num + 1));
        //Thread c3 = new Thread(new partidaC(num+2,juego1));
        Thread c4 = new Thread(new partidaC(num + 3));
        // Thread c5 = new Thread(new partidaC(num+4,juego2));
        //  Thread c6 = new Thread(new NextS(num+5));
        c1.start();
        c2.start();
        // c3.start();
        c4.start();
        //  c5.start();
        // c6.start();

    }
};

class partidaC implements Runnable {

    int num;
    Move mov = null;

    partidaC(int n) {
        this.num = n;
    }

    public void run() {
        try {
            ServerSocket s = new ServerSocket(num);
            ServerSocket s2 = new ServerSocket(num + 1);
            for (;;) {//ciclo infinito
                //Conexion jugador 1
                Socket cl = s.accept();
                System.out.println("Conexión establcida desde " + cl.getInetAddress() + ":" + cl.getPort());
                String mensaje = "Hola jugador-1";
                PrintWriter pw = new PrintWriter(new OutputStreamWriter(cl.getOutputStream()));
                pw.flush();
                pw.println(mensaje);
                pw.flush();
                Socket c2 = s2.accept();
                //conexion jugador 2
                System.out.println("Conexión establcida desde " + c2.getInetAddress() + ":" + c2.getPort());
                mensaje = "Hola jugador-2";
                PrintWriter pw2 = new PrintWriter(new OutputStreamWriter(c2.getOutputStream()));
                pw2.flush();
                pw2.println(mensaje);
                pw2.flush();//le dice al jugador 2 que es el jugador 2
                BufferedReader br2 = new BufferedReader(new InputStreamReader(cl.getInputStream()));
                BufferedReader br22 = new BufferedReader(new InputStreamReader(c2.getInputStream()));
                while (true) {
                    String mensajec = br2.readLine();//lee mensaje del jugador 1
                    System.out.println("Mensajej1: " + mensajec);
                    String[] coordenada = mensajec.split("-");
                    mov = new Move(Integer.parseInt(coordenada[0]), Integer.parseInt(coordenada[1]));
                    pw2.flush();//envia a jugador 2
                    pw2.println(mensajec);
                    pw2.flush();
                    System.out.println("Se envio jugada al j2");
                    mensajec = br22.readLine();//lee mensaje del jugador 2
                    System.out.println("Se recivio jugada del j2");
                    System.out.println("Mensajej2: " + mensajec);
                    coordenada = mensajec.split("-");
                    mov = new Move(Integer.parseInt(coordenada[0]), Integer.parseInt(coordenada[1]));
                    pw.flush();//envia a jugador 1
                    pw.println(mensajec);
                    pw.flush();

                }
                //se limpia el flujo
                pw.close();
                cl.close();
            }//for
        }//try
        catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error");
        }//try catch
    }
}

//class partidaCj2 implements Runnable {
//
//    int num;
//
//    partidaCj2(int n) {
//        this.num = n;
//    }
//
//    public void run() {
//        try {
//            ServerSocket s = new ServerSocket(num);
//            System.out.println("Esperando jugador 2");
//            for (;;) {//ciclo infinito
//                //bloqueo
//                Socket cl = s.accept();
//                System.out.println("Conexión establcida desde " + cl.getInetAddress() + ":" + cl.getPort());
//
//                String mensaje = "Hola jugador-2";
//                PrintWriter pw = new PrintWriter(new OutputStreamWriter(cl.getOutputStream()));
//                //se envia l mensaje
//                pw.flush();
//                pw.println(mensaje);
//                pw.flush();
//                BufferedReader br2 = new BufferedReader(new InputStreamReader(cl.getInputStream()));
//                String mensajec = br2.readLine();
//                System.out.println("Recibimos un mensaje desde el cliente");
//                System.out.println("Mensaje: " + mensajec);
//                //se limpia el flujo
//                pw.close();
//                cl.close();
//            }//for
//        }//try
//        catch (Exception e) {
//            e.printStackTrace();
//            System.err.println("Error");
//        }//try catch
//    }
//}

class Conexiones implements Runnable {

    int num;
    int Cjuegos = 0;

    Conexiones(int n) {
        this.num = n;
    }

    public void run() {
        try {
            ServerSocket s = new ServerSocket(num);
            System.out.println("Esperando clientes...");
            for (;;) {//ciclo infinito
                //bloqueo
                Socket cl = s.accept();
                System.out.println("Conexión establcida desde " + cl.getInetAddress() + ":" + cl.getPort());
                String mensaje;
                if (Cjuegos > 3) {
                    mensaje = "Servidor lleno intenta con el puerto-" + num + 5;
                } else {
                    Cjuegos++;
                    mensaje = "Conectandose al juego-" + (cl.getLocalPort() + Cjuegos);
                }//mensaje //puerto al cual se conectara 
                PrintWriter pw = new PrintWriter(new OutputStreamWriter(cl.getOutputStream()));
                pw.flush();
                pw.println(mensaje);
                pw.flush();
                //  pw.flush();
                // pw.println(mensaje);
                // pw.flush();
                pw.close();
                cl.close();
            }//for
        }//try
        catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error");
        }//try catch
    }
}

class NextS implements Runnable {

    int num;

    NextS(int n) {
        this.num = n;
    }

    public void run() {
        try {
            ServerSocket s = new ServerSocket(num);
            System.out.println("Esperando cliente...");
            for (;;) {//ciclo infinito
                //bloqueo
                Socket cl = s.accept();
                System.out.println("Conexión establcida desde " + cl.getInetAddress() + ":" + cl.getPort());
                String mensaje = "Soy el siguiente servidor";
                PrintWriter pw = new PrintWriter(new OutputStreamWriter(cl.getOutputStream()));
                //se envia l mensaje
                pw.flush();
                pw.println(mensaje);
                pw.flush();
                BufferedReader br2 = new BufferedReader(new InputStreamReader(cl.getInputStream()));
                String mensajec = br2.readLine();
                System.out.println("Recibimos un mensaje desde el cliente");
                System.out.println("Mensaje: " + mensajec);
                //se limpia el flujo
                pw.close();
                cl.close();
            }//for
        }//try
        catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error");
        }//try catch
    }
}
