package multicast;

import java.net.*;
import java.io.*;

public class CMulticastB {

    public static void main(String[] args) {
        InetAddress gpo = null;
        try {
            BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
            MulticastSocket cl = new MulticastSocket(9999);
            System.out.println("Cliente Escuchando puerto" + cl.getLocalPort());
            cl.setReuseAddress(true);
            try {
                gpo = InetAddress.getByName("228.1.1.1");
            } //try
            catch (UnknownHostException u) {
                System.err.println("Dirección errónea");
            } //catch
            cl.joinGroup(gpo);
            System.out.println("Unido al grupo\n Ingrese un mensaje para enviar");
            String msj2 = br1.readLine();
            for (;;) {
                DatagramPacket p = new DatagramPacket(new byte[10], 10);
                cl.receive(p);
                String msj = new String(p.getData());
                System.out.println("Datagrama recibido: " + msj);
                System.out.println("Servidor descubierto: " + p.getAddress() + ": " + p.getPort());
                byte[] b = msj2.getBytes();
                DatagramPacket q = new DatagramPacket(b, b.length, gpo, 9999);
                cl.send(q);
                try {
                    Thread.sleep(3000); //3seg
                } //try
                catch (InterruptedException ie) {
                    ie.printStackTrace();
                }
            } //for
        } //try
        catch (Exception e) {
            e.printStackTrace();
        } //catch        
    } //main    
} //class
