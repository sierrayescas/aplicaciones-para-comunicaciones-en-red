/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multicast;

import java.net.*;
import java.io.*;
/**
 *
 * @author Eduardo
 */
public class CMulticastBEscucha {
    public static void main(String[] args) {
        InetAddress gpo=null;
        try{
            BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
            MulticastSocket cl=new MulticastSocket(9999);
            System.out.println("Cliente Escuchando puerto" + cl.getLocalPort());
            cl.setReuseAddress(true);
            try{
                gpo=InetAddress.getByName("228.1.1.1");
            } //try
            catch(UnknownHostException u){
                System.err.println("Dirección errónea");
            } //catch
            cl.joinGroup(gpo);
            System.out.println("Unido al grupo");
            for(;;){
                DatagramPacket p= new DatagramPacket(new byte[10],10);
                cl.receive(p);
                String msj=new String(p.getData());
                System.out.println("Datagrama recibido: " + msj);
                System.out.println("Servidor descubierto: " + p.getAddress() + ": " + p.getPort());
            } //for
        } //try
        catch(Exception e){
            e.printStackTrace();
        } //catch        
    } //main    
} //class