/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hola.mundo.eco;
import java.net.*;
import java.io.*;
/**
 *
 * @author esierray1500
 */
public class HolaMundoEco {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try{
            ServerSocket s = new ServerSocket(1234);
            System.out.println("Esperando cliente...");
            for(;;){//ciclo infinito
                //bloqueo
                Socket cl = s.accept();
                System.out.println("Conexión establcida desde "+ cl.getInetAddress()+":"+cl.getPort());
                
                String mensaje = "hola mundo, soy yescas";
                PrintWriter pw = new PrintWriter(new OutputStreamWriter(cl.getOutputStream()));
                
                //se envia l mensaje
                pw.flush();
                pw.println(mensaje);
                pw.flush();
                BufferedReader br2 = new BufferedReader(new InputStreamReader(cl.getInputStream()));
                String mensajec = br2.readLine();
                System.out.println("Recibimos un mensaje desde el cliente");
                System.out.println("Mensaje: "+ mensajec);
                //se limpia el flujo
                pw.close();
                cl.close();
            }//for
        }//try
        catch(Exception e){
            e.printStackTrace();
            System.err.println("Error");
        }//try catch
    }//main
    
}//class
