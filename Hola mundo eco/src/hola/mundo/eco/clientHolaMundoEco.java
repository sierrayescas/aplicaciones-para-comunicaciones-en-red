/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hola.mundo.eco;
import java.io.*;
import java.net.*;
/**
 *
 * @author esierray1500
 */
public class clientHolaMundoEco {
    
    public static void main(String[] args) {
                try{
                    BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
                    System.out.println("Escribe la direccion del servidor");
                    String host = br1.readLine();
                    System.out.printf("\n\nEscriba el puerto ");
                    int pto = Integer.parseInt(br1.readLine());
                    //creamos el socket y nos conectamos
                    Socket cl = new Socket(host,pto);
                    
                    BufferedReader br2 = new BufferedReader(new InputStreamReader(cl.getInputStream()));
                    //nos conectamos
                    String mensaje = br2.readLine();
                    System.out.println("Recibimos un mensaje desde el servidor");
                    System.out.println("Mensaje: "+ mensaje);
                    PrintWriter pw = new PrintWriter(new OutputStreamWriter(cl.getOutputStream()));
                    //cerramos los flujos y el socket
                    System.out.println("Escriba un mensaje para enviar al servidor");
                    mensaje = br1.readLine();
                    pw.flush();
                    pw.println(mensaje);
                    pw.flush();
                    pw.close();
                    br1.close();
                    br2.close();
                    cl.close();
                }//try
                catch(Exception e){
                    e.printStackTrace();
                    System.err.println("Error");
                }//try catch
    }//main
}//class
