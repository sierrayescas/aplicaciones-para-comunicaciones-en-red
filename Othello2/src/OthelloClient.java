/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.net.*;
import java.io.*;
import java.util.*;


/**
 *
 * @author Eduardo
 */
public class OthelloClient {

    public static void main(String[] args) {
        try {
            BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Escribe la direccion del servidor");
            String host = br1.readLine();
            System.out.println("Escriba el puerto ");
            int pto = Integer.parseInt(br1.readLine());
            //creamos el socket y nos conectamos
            Socket cl = new Socket(host, pto);
            BufferedReader br2 = new BufferedReader(new InputStreamReader(cl.getInputStream()));
            //nos conectamos
            String mensaje = br2.readLine();
            System.out.println("Mensaje: " + mensaje);
            br1.close();
            br2.close();
            cl.close();
            String[] msj2 = mensaje.split("-");
            if ("Conectandose al juego".equals(msj2[0])) {
                cl = new Socket(host, Integer.parseInt(msj2[1]));
                br2 = new BufferedReader(new InputStreamReader(cl.getInputStream()));
                //nos conectamos
                mensaje = br2.readLine();
                System.out.println("Mensaje: " + mensaje);
                msj2 = mensaje.split("-");
                Othello app;
                if ("1".equals(msj2[1])) {
                    app = new Othello(true);
                } else {
                    app = new Othello(false);
                }
                PrintWriter pw = new PrintWriter(new OutputStreamWriter(cl.getOutputStream()));
                while (true) {
                    if (app.getGPanel().getUltimo() != null) {
                        mensaje = "" + app.getGPanel().getUltimo().getI() + "-" + app.getGPanel().getUltimo().getJ();
                        app.getGPanel().rUltimo();
                        pw.flush();
                        pw.println(mensaje);
                        pw.flush();
                        mensaje = br2.readLine();
                        System.out.println("Mensaje: " + mensaje);
                        msj2 = mensaje.split("-");
                    }
                    Thread.sleep(1000);
                }
                br1.close();
                br2.close();
                pw.close();
                cl.close();
            }
        }//try
        catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error");
        }//try catch
    }//main
}
