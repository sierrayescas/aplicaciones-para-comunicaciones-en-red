/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Eduardo
 */
public class SeriPal {
   String pal;
   int stat[];

    public SeriPal(String pal) {
        int i;
        this.pal = pal;
        this.stat = new int[pal.length()];
        for(i=0;i<pal.length();i++){
            this.stat[i]=0;
        }
    }

    public String getPal() {
        return pal;
    }

    public int[] getStat() {
        return stat;
    }

    public void setStat(int[] stat) {
        this.stat = stat;
    }
    
   
}
