struct entrada{
	float arg1;
	float arg2;
};

program CALCULADORA{
	version CALCULADORA_VER{
		int sumar(entrada)=1;
		int restar(entrada)=2;
		float multiplicar(entrada)=3;
		float dividir(entrada)=4;
		float seno(entrada)=5;
		float coseno(entrada)=6;
	}=1;
}=0x30000001;
