struct entrada{
	int arg1;
	int arg2;
};
program calculadora{
	version calculadora_ver{
		int suma(entrada)=1;
		int resta(entrada)=2;
		int multiplicar(entrada)=3;
		int dividir(entrada)=4;
		}=1;
}=0x30000001;