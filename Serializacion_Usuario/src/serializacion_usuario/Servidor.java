/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializacion_usuario;
import java.net.*;
import java.io.*;
/**
 *
 * @author esierray1500
 */
public class Servidor {
    public static void main(String[] args) {
        ObjectOutputStream oos = null;
        ObjectInputStream ois= null;
        try {
            ServerSocket s = new ServerSocket(9999);
            System.out.println("Servidor iniciado... ");
            for(;;){
                Socket cl = s.accept();
                System.out.println("Cliente conectado desdde "+ cl.getInetAddress() + ":"+cl.getPort());
                oos = new ObjectOutputStream(cl.getOutputStream());
                ois = new ObjectInputStream(cl.getInputStream());
                Serializacion_Usuario u = (Serializacion_Usuario)ois.readObject();//**
                System.out.println("Objeto recibido... Extrayendo informacion");
                System.out.println("nombre "+ u.getNombre());
                System.out.println("A.paterno "+ u.getApaterno());
                System.out.println("A.materno "+ u.getAmaterno());
                System.out.println("Password "+ u.getPwd());
                System.out.println("Edad "+ u.getEdad());
                System.out.println("Devolviendo objeto...");
                oos.writeObject(u);
                oos.flush();
            }   
        } catch (Exception e) {
            e.printStackTrace();
        }
   
    }
    
}
