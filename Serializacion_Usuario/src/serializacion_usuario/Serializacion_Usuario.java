/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializacion_usuario;

import java.io.Serializable;

/**
 *
 * @author esierray1500
 */
public class Serializacion_Usuario implements Serializable {

    String nombre;
    String apaterno;
    String amaterno;
    transient String pwd;
    int edad;

    public String getAmaterno() {
        return amaterno;
    }

    public String getApaterno() {
        return apaterno;
    }

    public int getEdad() {
        return edad;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPwd() {
        return pwd;
    }

    /**
     * @param args the command line arguments
     */
    public  Serializacion_Usuario(String nombre, String apaterno, String amaterno, String pwd, int edad) {
        this.nombre = nombre;
        this.amaterno = amaterno;
        this.apaterno = apaterno;
        this.pwd = pwd;
        this.edad = edad;
    }

    public static void main(String[] args) {
        // TODO code application logic here
    }

}