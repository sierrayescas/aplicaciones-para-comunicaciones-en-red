/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serializacion_usuario;

import java.io.*;
import java.net.*;

/**
 *
 * @author esierray1500
 */
public class Cliente {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //creamos el socket
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;
        String host = "127.0.0.1";
        int port = 9999;
        try {
            Socket cl = new Socket(host, port);
            System.out.println("Conexion establecida... ");
            oos = new ObjectOutputStream(cl.getOutputStream());
            ois = new ObjectInputStream(cl.getInputStream());
            Serializacion_Usuario u = new Serializacion_Usuario("Pepito", "Perez", "Juarez", "12345", 20);
            System.out.println("Enviando objeto");
            oos.writeObject(u);
            oos.flush();
            System.out.println("Preparando para recibir respuesta... ");
            Serializacion_Usuario u2 = (Serializacion_Usuario) ois.readObject();
            System.out.println("Objeto recibido... Extrayendo datos.");
            System.out.println("nombre " + u2.getNombre());
            System.out.println("A.paterno " + u2.getApaterno());
            System.out.println("A.materno " + u2.getAmaterno());
            System.out.println("Password " + u2.getPwd());
            System.out.println("Edad " + u2.getEdad());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
