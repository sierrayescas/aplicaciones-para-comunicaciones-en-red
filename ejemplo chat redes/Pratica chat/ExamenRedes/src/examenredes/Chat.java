package examenredes;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;




public class Chat extends javax.swing.JFrame{
    private String user;
    Client cliente;
    String[] conectados;
    String textoChat="";
    String linea="";
    public static String formattedDate;
    Highlighter hilit;
    Highlighter.HighlightPainter painter;
    int extras=0;
    //String Perros=CargarTema("C:\\Users\\leona\\Desktop\\Redes_Enrique}\\Examen1parcial\\Perros.txt");
    //String Carros=CargarTema("C:\\Users\\leona\\Desktop\\Redes_Enrique}\\Examen1parcial\\Carros.txt");
    List<String> listaConectados;
    String privateTo="";
    DefaultListModel modelo;
    int selected;
    ArrayList<String> TemaType = new ArrayList<String>();
    ArrayList<String> TemaPlain = new ArrayList<String>();
    ArrayList<String> Paths = new ArrayList<String>();
    ArrayList<String> PathsPlain = new ArrayList<String>();
    
    public Chat() {
        initComponents();
        txtChat.setContentType("text/html");
        txtChat.setEditable(false);
        txtChat.setText("");
        listaConectados=new ArrayList<>();
        
        String sDirectorio = "C:\\Users\\alumno\\Documents\\NetBeansProjects\\ExamenRedes\\Temas";
        
        File f = new File(sDirectorio);
        
        ArrayList<String> listtxt = new ArrayList<String>();
        if (f.exists()){ 
              JOptionPane.showMessageDialog(null, "Directorio existe");
        }// Directorio existe }
else { //Directorio no existe }
         JOptionPane.showMessageDialog(null, "Directorio NO existe!!");
        } 
        File[] ficheros = f.listFiles();
        DefaultListModel list = new DefaultListModel();
        
            for (int x=0;x<ficheros.length;x++)
                {
                    System.out.println(ficheros[x].getName());
                    list.add(x, ficheros[x].getName());
                    TemaType.add(CargarTema(ficheros[x].getAbsolutePath()));
                    TemaPlain.add(CargarTema("/C:/Users/alumno/Documents/NetBeansProjects/ExamenRedes/PlainTxt/"+ficheros[x].getName()));
                    Paths.add(ficheros[x].getAbsolutePath());
                    PathsPlain.add("/C:/Users/alumno/Documents/NetBeansProjects/ExamenRedes/PlainTxt/"+ficheros[x].getName());
                }  
        Temas.setModel(list);
        //TemaType.add(Perros);
        //TemaType.add(Carros);
        
        //Paths.add("C:\\Users\\leona\\Desktop\\Redes_Enrique}\\Examen1parcial\\Perros.txt");
        //Paths.add("C:\\Users\\leona\\Desktop\\Redes_Enrique}\\Examen1parcial\\Carros.txt");
        
        cliente = new Client(this);
        user = JOptionPane.showInputDialog(this, "Nombre");
        if(user==null)
            System.exit(0);
        cliente.start();
        cliente.envia("<inicio> "+user);
        //cliente.envia("<msg> <b>"+user+"</b> se ha conectado.");
        txtEstado.setText("Usuario "+user);
        
        listConectados.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent evt) {
                JList list =(JList)evt.getSource();
                if (evt.getClickCount() == 2) {
                    int index = list.locationToIndex(evt.getPoint());
                    privateTo = (String)modelo.getElementAt(index);
                    String texto = JOptionPane.showInputDialog("Mensaje privado a " + privateTo);
                    
                    if(texto!=null)
                        cliente.envia("<privado><"+user+"><"+privateTo+"> "+texto);
                } else if (evt.getClickCount() == 3) {   // Triple-click
                    int index = list.locationToIndex(evt.getPoint());
                    privateTo = (String)modelo.getElementAt(index);
                }
            }
        });
        
        Temas.addMouseListener(new MouseAdapter(){
           public void mouseClicked(MouseEvent evt)
           {
               //JOptionPane.showMessageDialog(Temas, Temas.getSelectedValue());
               selected = Temas.getSelectedIndex();
               
               txtChat.setText(TemaType.get(selected));
               textoChat="";
               
           }
        });
    }

    public void updateUsersList(String usersCSV){
        String[] users = usersCSV.split(",");
        modelo = new DefaultListModel();
        listConectados.removeAll();
        
        for(String user:users){
            user.replace(" ","");
            user.trim();
            if(!user.contains("Conectados") || user.isEmpty())
                modelo.addElement(user);
        }
        listConectados.setModel(modelo);
    }
    public void Agregar_conversacion(String msg){
        String senderUser="";
        String toUser="";
        System.out.println(msg);
        String aux=TemaPlain.get(selected);
        
        textoChat = TemaType.get(selected);
              
        msg = msg.replaceAll(":D", "<img width=\"50\" height=\"50\" src=\"file:./feliz.png\"></img>");
        msg = msg.replaceAll("<3", "<img width=\"50\" height=\"50\" src=\"file:./enamorado.png\"></img>");       
        msg = msg.replaceAll(":c", "<img width=\"50\" height=\"50\" src=\"file:./llorar.png\"></img>");
        msg = msg.replaceAll(":3", "<img width=\"50\" height=\"50\" src=\"file:./tierna.png\"></img>");
       
        if(msg.contains("$img$"))
        {
            senderUser=msg.split("<")[2].split(">")[0];
            String[] nameF = msg.split(Pattern.quote("$"));
           
                
            msg = "<img width=\"50\" height=\"50\" src=\"file:/C:/Users/alumno/Documents/NetBeansProjects/ExamenRedes/Imagenes/"+formattedDate+"/"+nameF[3]+"\"></img>";
            if(senderUser.equals("b"))
            {
                textoChat=textoChat+"<br/><br/>"+msg;
                aux += msg;
            }
            else
            {
                textoChat=textoChat+"<br/><br/><b>"+senderUser+"</b>:<br/> "+msg;
                aux += senderUser+msg.split(">")[2];
            }
            txtChat.setText(textoChat);
            TemaType.add(selected, textoChat);
            TemaPlain.add(selected,aux);
            EscribirDatos(Paths.get(selected),TemaType.get(selected));
            EscribirDatos(PathsPlain.get(selected),TemaPlain.get(selected));
        }
        else
        {
        if(msg.contains("<msg>")){
            senderUser=msg.split("<")[2].split(">")[0];
            if(senderUser.equals("b"))
            {
                textoChat=textoChat+""+msg;
                aux += msg;
                System.out.println("mensaje if b : "+msg);
            }
            else
            {
                textoChat=textoChat+"<b>"+senderUser+"</b>:<br/> "+msg;
                aux += senderUser+msg.split(">")[2];
                System.out.println("mensaje else : "+senderUser+msg.split(">")[2]);
            }
            LocalDateTime myDateObj = LocalDateTime.now();
            DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss"); 
            String formattedDate = myDateObj.format(myFormatObj); 
            txtChat.setText(textoChat+formattedDate);
            TemaType.add(selected, textoChat+formattedDate);
            TemaPlain.add(selected, aux+formattedDate);
            EscribirDatos(Paths.get(selected),TemaType.get(selected));
            EscribirDatos(PathsPlain.get(selected),TemaPlain.get(selected));
        }else if(msg.contains("<inicio>")){
            msg.replace("<inicio>", "");
            listaConectados.add(msg);
        }
        else if(msg.contains("<conectados")){
            updateUsersList(msg);
        }else if(msg.contains("privado")){
            senderUser=msg.split("<")[2].replace(">", "");
            toUser=msg.split("<")[3].split(">")[0];
            if(toUser.equals(user))
            {
               textoChat=textoChat+"<br/><br/><b>Mensaje privado de "+senderUser+"</b>:<br/>"+msg;
               txtChat.setText(textoChat);
               
            }
        }
        }
    }
    
    public String CargarTema(String path)
    {
         File archivo = null;
      FileReader fr = null;
      BufferedReader br = null;
        System.out.println("ruta: "+path);
      String aux="";
      linea="";
      try {
         // Apertura del fichero y creacion de BufferedReader para poder
         // hacer una lectura comoda (disponer del metodo readLine()).
         archivo = new File (path);
         fr = new FileReader (archivo);
         br = new BufferedReader(fr);

         // Lectura del fichero
         
         while((aux=br.readLine())!=null)
         {
            linea += aux;
         }
        
      }
      catch(Exception e){
         e.printStackTrace();
      }finally{
         // En el finally cerramos el fichero, para asegurarnos
         // que se cierra tanto si todo va bien como si salta 
         // una excepcion.
         try{                    
            if( null != fr ){   
               fr.close();     
            }                  
         }catch (Exception e2){ 
            e2.printStackTrace();
         }
      }
      
        System.out.println(linea+"\n");
         return linea;
    }
    
    public void EscribirDatos(String path,String msg)
    {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter(path);
            pw = new PrintWriter(fichero);

            pw.println(msg);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
    }
    
private static void MandarImagen(File source, File dest) throws IOException{
    InputStream is = null;
    OutputStream os = null;
    try {
        
        is = new FileInputStream(source);
        os = new FileOutputStream(dest);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = is.read(buffer)) > 0) {
            os.write(buffer, 0, length);
        }
    } catch (Exception e) {
            e.printStackTrace();
        }
    finally {
        is.close();
        os.close();
    }
}


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane2 = new javax.swing.JScrollPane();
        listConectados = new javax.swing.JList();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtEnvia = new javax.swing.JTextArea();
        btnEnviar = new javax.swing.JButton();
        txtEstado = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtChat = new javax.swing.JEditorPane();
        jScrollPane4 = new javax.swing.JScrollPane();
        Temas = new javax.swing.JList<>();
        Imagen = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setBackground(new java.awt.Color(51, 51, 51));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        listConectados.setBackground(new java.awt.Color(153, 153, 153));
        jScrollPane2.setViewportView(listConectados);

        txtEnvia.setBackground(new java.awt.Color(51, 51, 51));
        txtEnvia.setColumns(20);
        txtEnvia.setFont(new java.awt.Font("Monospaced", 1, 13)); // NOI18N
        txtEnvia.setForeground(new java.awt.Color(255, 255, 255));
        txtEnvia.setRows(5);
        jScrollPane3.setViewportView(txtEnvia);

        btnEnviar.setBackground(new java.awt.Color(0, 51, 255));
        btnEnviar.setFont(new java.awt.Font("Kristen ITC", 1, 11)); // NOI18N
        btnEnviar.setForeground(new java.awt.Color(51, 51, 51));
        btnEnviar.setText("Enviar");
        btnEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnviarActionPerformed(evt);
            }
        });

        txtEstado.setText("Estado: Desconectado");

        jScrollPane1.setViewportView(txtChat);

        Temas.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        Temas.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        Temas.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Perros", "Carros" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane4.setViewportView(Temas);

        Imagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/examenredes/subir.png"))); // NOI18N
        Imagen.setText("Imagen");
        Imagen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ImagenActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane5.setViewportView(jTextArea1);

        jLabel1.setText("Temas:");

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/examenredes/find.png"))); // NOI18N
        jButton1.setText("Buscar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Imagen, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnEnviar, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(128, 128, 128)
                        .addComponent(txtEstado))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(39, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtEstado)
                .addGap(3, 3, 3)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 220, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(Imagen)
                                .addGap(18, 18, 18)
                                .addComponent(btnEnviar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jButton1))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnviarActionPerformed
        // TODO add your handling code here:

        String texto=txtEnvia.getText();
        cliente.envia("<msg><"+user+">"+texto);
        txtEnvia.setText("");
    }//GEN-LAST:event_btnEnviarActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
        //cliente.envia("<fin>"+user);
    }//GEN-LAST:event_formWindowClosed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:
        cliente.envia("<fin>"+user);
        System.exit(0);
    }//GEN-LAST:event_formWindowClosing

    private void ImagenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ImagenActionPerformed
        // TODO add your handling code here:
        JFileChooser jf = new JFileChooser();
        jf.showOpenDialog(null);
        File f = jf.getSelectedFile();
        try {
             LocalDateTime myDateObj = LocalDateTime.now();
            DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("dd-MM-yyyy"); 
            formattedDate = myDateObj.format(myFormatObj); 
            File file = new File("/C:/Users/alumno/Documents/NetBeansProjects/ExamenRedes/Imagenes/"+formattedDate);
            
            if(!file.exists())
            {
                System.out.println("No existe");
                if(file.mkdir())
                    System.out.println("Directorio creado"+file.getAbsolutePath());
            }
            MandarImagen(f,new File("C:\\Users\\alumno\\Documents\\NetBeansProjects\\ExamenRedes\\Imagenes\\"+formattedDate+"\\"+f.getName()));
        } catch (IOException ex) {
            Logger.getLogger(Chat.class.getName()).log(Level.SEVERE, null, ex);
        }
        cliente.envia("<msg><"+user+">"+"$img$$"+f.getName()+"$");
    }//GEN-LAST:event_ImagenActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        String texto=jTextArea1.getText();
        Buscar(texto);
    }//GEN-LAST:event_jButton1ActionPerformed


    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Chat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Chat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Chat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Chat.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        System.setProperty("java.net.preferIPv4Stack", "true");
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Chat().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Imagen;
    private javax.swing.JList<String> Temas;
    private javax.swing.JButton btnEnviar;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JList listConectados;
    private javax.swing.JEditorPane txtChat;
    private javax.swing.JTextArea txtEnvia;
    private javax.swing.JLabel txtEstado;
    // End of variables declaration//GEN-END:variables

    private void Buscar(String texto) {
        
         //Color colorfondodefault= jTextArea1.getBackground();
        // String palabra;
        //String s=txtChat.getText();
        //String contenido = texto.getText(); 
        //int end = index + texto.length();
        //int startPos=texto.length(), endPos=0;
        
        System.out.println("palabra: "+texto+"\n");

         //String contenido=txtChat.getText();
         //txtChat.setText(textoChat);TemaType.add(selected, textoChat);txtChat.setContentType("text/html");
         String contenido= TemaPlain.get(selected);
         
         System.out.println("cadena contenido: "+contenido);
         //txtChat.setText(contenido);
            int index = contenido.indexOf(texto, 0);
            index += 5;
             hilit = new DefaultHighlighter();
             painter = new DefaultHighlighter.DefaultHighlightPainter(Color.CYAN);
             txtChat.setHighlighter(hilit);
            if (index >= 0) {
                
                  try
                   {      //index-(4*21)
                       int end = index + texto.length();
                       hilit.addHighlight(index, end, painter);
                       txtChat.setCaretPosition(end);
                      /* javax.swing.text.DefaultHighlighter.DefaultHighlightPainter highlightPainter = new javax.swing.text.DefaultHighlighter.DefaultHighlightPainter(Color.YELLOW);
                       txtChat.getHighlighter().addHighlight(index,end,highlightPainter);*/
                       System.out.println("index: "+index+"\nend"+end+"\n");
                   }
                   catch(Exception ex)
                   {
                   }  
            } else {
                System.out.println("no existe la palabra");
                JOptionPane.showMessageDialog(this, "No existe la palabra");
                }
        
         /*int index = s.indexOf(texto);
            if (index >= 0)      
       
    */      
            
            
    }

   
}

