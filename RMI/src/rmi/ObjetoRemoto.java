/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmi;
import java.io.Serializable; 
import java.rmi.server.UnicastRemoteObject;
/**
 *
 * @author Eduardo
 */

public class ObjetoRemoto extends UnicastRemoteObject implements InterfaceRemota 
{ 
    public ObjetoRemoto()throws Exception{
        ;
    }
    @Override
    public int suma(int a, int b) throws java.rmi.RemoteException 
    { 
        System.out.println ("sumando " + a + " + " + b + "..."); 
        return a+b; 
    }

   
    @Override
    public int resta(int a, int b) throws java.rmi.RemoteException
    {
        System.out.println ("restando " + a + " - " + b + "..."); 
        return a-b; 
    }

    
    @Override
    public float mult(float a, float b)throws java.rmi.RemoteException
    {
       System.out.println ("multiplicando " + a + " x " + b + "..."); 
        return a*b; 
    }

    
    @Override
    public float div(float a, float b)throws java.rmi.RemoteException
    {
        System.out.println ("Dividiendo " + a + " / " + b + "..."); 
        return a/b; 
    }

    
    @Override
    public float seno(float a, float b)throws java.rmi.RemoteException
    {
        double degrees = (double)b;
        double radians = Math.toRadians(degrees);
        System.out.println (" " + a + "Sin(" + b + ")..."); 
        return (float)(a*Math.sin(radians)); 
    }

    
    @Override
    public float coseno(float a, float b)throws java.rmi.RemoteException
    {
        double degrees = (double)b;
        double radians = Math.toRadians(degrees);
        System.out.println (" " + a + "Cos(" + b + ")..."); 
        return (float)(a*Math.cos(radians)); 
    }
}
