/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmi;

/**
 *
 * @author Eduardo
 */
import java.rmi.Remote; 
public interface InterfaceRemota extends Remote 
{ 
    public int suma (int a, int b) throws java.rmi.RemoteException; 
    public int resta (int a, int b) throws java.rmi.RemoteException; 
    public float mult (float a, float b) throws java.rmi.RemoteException; 
    public float div (float a, float b) throws java.rmi.RemoteException; 
    public float seno (float a, float b) throws java.rmi.RemoteException; 
    public float coseno (float a, float b) throws java.rmi.RemoteException; 
}
