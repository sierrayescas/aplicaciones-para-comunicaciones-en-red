
import java.io.Externalizable;
import java.io.*;
import java.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author esierray1500
 */
class Usuario implements Externalizable {

    private String usuario;
    private String password;

    public Usuario() {
        System.out.println("Creando usuario");
    }

    public Usuario(String usuario, String password) {
        System.out.println("Creando usuario " + usuario + " (" + password + ")");
        this.usuario = usuario;
        this.password = password;
    }

    public void writeExternal(ObjectOutput out) throws IOException {
        //ex´ñicitamente indicamos cuales atriburos se van a enviar
        out.writeObject(usuario);
    }

    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        System.out.println("Usuario.readExternal");
        //explicitamente indicamos cuales atributos se van a recuperar
        usuario = (String) in.readObject();
    }

    public void muestraUsuario() {
        String cad = "Usuario: " + usuario + "Pasword: ";
        if (password == null) {
            cad = cad + "No disponible";
        } else {
            cad = cad + " " + password;
        }
        System.out.println(cad);
    }
    public void muestraUsuarios(){
        ListIterator li = lista.listIterator();
        Usuario u;
        while(li.hasNext()){
            u = (Usuario)li.next();
            u.muestraUsuario();
        }
    }
    class ListaUsuarios implements Serializable{
        private LinkedList lista = new LinkedList();
        int valor;

        public ListaUsuarios(String[Usuario],String password) {
            for(i=0;i<usuarios.length, i++)
                lista.add(new Usuario[usuario[i],password[i]])
        }
    }
}
