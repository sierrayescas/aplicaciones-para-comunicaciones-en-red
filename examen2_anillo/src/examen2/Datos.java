/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen2;

import java.io.Serializable;

/**
 *
 * @author alexis
 */
public class Datos implements Serializable {

    private int tipo;
    private String busqueda;
    private String origen;
    private String destino = "";
    private String hash="";
    private int puerto, puertoflujo;

    public Datos(int tipo, String busqueda, String origen, int puerto, int puertoflujo) {
        this.tipo = tipo;
        this.busqueda = busqueda;
        this.origen = origen;
        this.puerto = puerto;
        this.puertoflujo = puertoflujo;
    }

    public Datos() {
    }

    public Datos(String origen, String destino, int puerto) {
        this.origen = origen;
        this.destino = destino;
        this.puerto = puerto;
    }

    public Datos(String busqueda, String origen) {
        this.busqueda = busqueda;
        this.origen = origen;
    }

    public Datos(int tipo, String origen, String destino) {
        this.destino = destino;
        this.origen = origen;
        this.tipo = tipo;

    }

    public Datos(int tipo, String origen, String destino, String busqueda) {
        this.destino = destino;
        this.origen = origen;
        this.tipo = tipo;
        this.busqueda = busqueda;
    }

    public Datos(int tipo, String id) {
        this.tipo = tipo;
        this.origen = id;
    }

    public int getTipo() {
        return tipo;
    }

    public String getBusqueda() {
        return busqueda;
    }

    public String getOrigen() {
        return origen;
    }

    public String getDestino() {
        return destino;
    }
    
    public String gethHash() {
        return hash;
    }
    

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public void setBusqueda(String busqueda) {
        this.busqueda = busqueda;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public int getPuerto() {
        return puerto;
    }

    public void setPuerto(int puerto) {
        this.puerto = puerto;
    }

    public int getPuertoflujo() {
        return puertoflujo;
    }

    public void setPuertoflujo(int puertoflujo) {
        this.puertoflujo = puertoflujo;
    }

    public void sethHash(String hash) {
        this.hash = hash;
    }

    
}
