/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen2;


import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;
import java.net.InetAddress;
import javax.swing.JTextPane;

/**
 *
 * @author alexis
 */
public class ThreadNodo extends Thread {

    InetAddress group;
    public static final String MCAST_ADDR = "228.1.1.1";
    public static final int MCAST_PORT = 4000;
    public static final int DGRAM_BUF_LEN = 512;
    MulticastSocket socket;
    static JTextPane je1;
    
    static JTextPane idant;
    static JTextPane idsig;
    static String id;
    String mjs;
    Datos dts;
    int[] ids_nodos;

    public ThreadNodo(JTextPane pane, JTextPane ant, JTextPane sig, String id) {
        this.idant = ant;
        this.idsig = sig;
        this.je1 = pane;
        this.id = id;
        ids_nodos= new int [1000];
        try {
            socket = new MulticastSocket(MCAST_PORT);
           socket.setReuseAddress(true);
            group = InetAddress.getByName(MCAST_ADDR);
            socket.joinGroup(group);
            System.out.println("Unido al grupo");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            int int_id = Integer.parseInt(id);
            
            for (;;) {
               
                byte[] buf = new byte[DGRAM_BUF_LEN];//crea arreglo de bytes 
                DatagramPacket recv = new DatagramPacket(buf, buf.length);//crea el datagram packet a recibir
                socket.receive(recv);// ya se tiene el datagram packet
                
                String m = new String(recv.getData(),0,recv.getLength());
                                
                     if(m.equals("1")){
                      if (int_id==9000) {
                           if(idant.getText().equals("")){
                               idant.setText(Integer.toString(recv.getPort()));}
                           else if(recv.getPort()>(Integer.parseInt(idant.getText()))){
                                
                        idant.setText(Integer.toString(recv.getPort()));}

                        if(idsig.getText().equals("")){
                            idsig.setText(Integer.toString(recv.getPort()));
                        }
                        else if((recv.getPort()-9000)==1){
                            idsig.setText(Integer.toString(recv.getPort()));
                        }
                    }
                    else{
                         
                        if(idsig.getText().equals("")){
                            idant.setText(Integer.toString(int_id-1));
                            idsig.setText("9000");
                             
                        }
                        else if((recv.getPort()-int_id)==1){
                           
                            idsig.setText(Integer.toString(int_id+1));
                        }
                    }
                      
                      if(!busqueda(ids_nodos,recv.getPort())){
                          ids_nodos[recv.getPort()-9000]=recv.getPort();
                          if(recv.getPort()!=int_id){
                              mjs = "nuevo servidor unido con id : 127.0.0.1:"+recv.getPort();
                             mjs = (je1.getText().equals("")) ? mjs : je1.getText()+"\n"+mjs;
                             je1.setText(mjs);
                          }
                      }
                  
                              
                              

                }
 
            }
            
        } catch (IOException e) {
       e.printStackTrace(); }
    }
    
    public boolean busqueda (int [] arr, int num){
        for ( int i=0;i<arr.length;i++){
            if(arr[i]==num){
                return true;
            }
        }
        
        return false;
    }
    
    

}
/*           */