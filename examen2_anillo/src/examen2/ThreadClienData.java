/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen2;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import javax.swing.JTextField;
import javax.swing.JTextPane;

/**
 *
 * @author alexis
 */
public class ThreadClienData extends Thread {
    String mjs;
    String id;
    JTextField name;
    int int_id;
    JTextPane idsig;
    int siguiente;
    static JTextPane log;
    public ThreadClienData(String id, JTextField name,JTextPane idsig,JTextPane log) {
        super();
        this.id=id;
        this.name=name;
        this.idsig=idsig;
        this.log=log;
    }
    
    @Override
    public void run(){
      
        try {
            int_id=Integer.parseInt(id);
            siguiente= Integer.parseInt(idsig.getText());
            DatagramSocket cliente = new DatagramSocket();
            Datos data = new Datos(name.getText(),this.id);
            name.setText("");
            data.setPuerto(siguiente);
            data.setDestino(id);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(data);
            oos.flush();
            byte [] b = baos.toByteArray();
            DatagramPacket paquete = new DatagramPacket(b,b.length,InetAddress.getLocalHost(),siguiente-1000);
            cliente.send(paquete);
            
            paquete = new DatagramPacket (new byte[512],512);
            //esperando respuesta del proximo nodo
            log.setText(log.getText()+"\n"+id+" pregunto a :"+siguiente+" si tiene el archivo: "+data.getBusqueda());
            
            cliente.receive(paquete);
            ByteArrayInputStream baosIn = new ByteArrayInputStream(paquete.getData());
            ObjectInputStream ois = new ObjectInputStream(baosIn);
//            while(!data.getBusqueda().equals("-1")){
            data = (Datos)ois.readObject();
//                log.setText(log.getText()+"\n"+siguiente+": contesto "+data.getBusqueda()+" es en el nodo: "+data.getOrigen());
//                log.setText(log.getText()+"\npuerto de servidor de flujo donde esta el archivo:"+Integer.toString(data.getPuertoflujo()));
//                log.setText(log.getText()+"\nHash del archivo:"+data.gethHash());
//            }
            if(!data.getBusqueda().equals("-1")){
                log.setText(log.getText()+"\n"+siguiente+": contesto "+data.getBusqueda()+" es en el nodo: "+data.getOrigen());
                log.setText(log.getText()+"\npuerto de servidor de flujo donde esta el archivo:"+Integer.toString(data.getPuertoflujo()));
                log.setText(log.getText()+"\nHash del archivo:"+data.gethHash());
            
            RecibeArchivo(data.getPuertoflujo());
                
           
            }
            else{
                System.out.println("archivo no encontrado");
                log.setText(log.getText()+" \nrespuesta de "+siguiente+" :"+data.getBusqueda()+" archivo no encontrado");
                
            }
 
            cliente.close();
        } catch (Exception ex) {ex.printStackTrace();
            
        }
        
    }
    
    public void RecibeArchivo(int pto){
        try {
            Socket cliente = new Socket("localhost",pto);
            DataInputStream dis = new DataInputStream(cliente.getInputStream());
                byte [] elbyte = new byte [1024];
                String nombre = dis.readUTF();
                System.out.println("recibiendo el archivo "+ nombre );
                mjs= log.getText()+"\nrecibiendo el archivo "+ nombre;
                log.setText(mjs);
                Long tam = dis.readLong();
                DataOutputStream dos = new DataOutputStream (new FileOutputStream("archivos/"+id+"/"+nombre));
                
                
                long recibidos = 0;
                int n, porcenaje;
                
                while (recibidos <tam){
                    n = dis.read(elbyte);
                    dos.write(elbyte, 0, n);
                    dos.flush();
                    recibidos=recibidos +n;
                    int porcentaje = (int)(recibidos*100/tam);
                    mjs= log.getText()+"\nrecibido"+porcentaje+"%";
                    log.setText(mjs);
                    
                }
                
                System.out.println("archivo recibido con exito ");
                mjs= log.getText()+"\narachivo recibido con exito";
                log.setText(mjs);
                dos.close();
                dis.close();
                cliente.close();
        } catch (IOException ex) {ex.printStackTrace();
        }
    }
    
}
