/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen2;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.JTextField;
import javax.swing.JTextPane;

/**
 *
 * @author alexis
 */
public class ThreadSerData extends Thread {
     String id,mjs;
     static JTextPane log;
    static JTextField nameArch;
    int int_id;
  
    JTextPane idsig;
   int siguiente;
    Datos data,data2,data3;
    public ThreadSerData(String id, JTextPane log,JTextField nameArch,JTextPane idsig) {
        super();
        this.id=id;
        this.log=log;
        this.nameArch=nameArch;
        this.idsig=idsig;
    }

    @Override
    public void run(){
        int_id=Integer.parseInt(id);
        
        try {
           
            DatagramSocket ServDatam = new DatagramSocket(int_id-1000);
           log.setText("");

            for(;;){
               
               
                byte[]buf = new byte[512];
                DatagramPacket paquete = new DatagramPacket(buf,buf.length);
                ServDatam.receive(paquete);
                ByteArrayInputStream baosIn = new ByteArrayInputStream(paquete.getData());
                ObjectInputStream ois = new ObjectInputStream(baosIn);
                 data = (Datos)ois.readObject();
                 siguiente = Integer.parseInt(idsig.getText());
                System.out.println("recibo solicitud del nodo :"+data.getOrigen());
                if(!data.getDestino().equals(id))
                {log.setText( log.getText()+"\nNodo: "+data.getOrigen()+" busca el archivo:"+data.getBusqueda());}
               
 
                if(buscarArchivo(data.getBusqueda())){//encuentra el archivo
                    String MD5A = MD5.getMD5Checksum("archivos/"+id+"/"+data.getBusqueda());
                    System.out.println(MD5A);
                    data2 = new Datos();
                    data2.setPuertoflujo(int_id+100);
                    data2.setBusqueda(data.getBusqueda());
                    data2.setOrigen(id);
                    data2.sethHash(MD5A);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ObjectOutputStream oos = new ObjectOutputStream(baos);
                    oos.writeObject(data2);
                    oos.flush();
                    byte [] b = baos.toByteArray();
                   DatagramPacket respuesta = new DatagramPacket(b,b.length,paquete.getAddress(),paquete.getPort());
                   ServDatam.send(respuesta);
                   log.setText(log.getText()+"\n"+data.getBusqueda()+"esta en este nodo con el servidor de flujo puerto :"+Integer.toString(int_id+100));
                   ServidorFlujo(int_id,data.getBusqueda());
                   //log.setText(mjs);
                   
                  
                }
                else if (!data.getDestino().equals(id)){//no encontro el archivo y no es un nodo destino
                    
                 
                 if(!data.getDestino().equals(Integer.toString(siguiente)))//existe el archvo en la red
                 { log.setText(log.getText()+"\n"+"no esta entonces le pregunto al suguiente nodo: "+Integer.toString(siguiente));}
                 else{//no existe el archivo en la red
                     log.setText(log.getText()+"\n archivo no esta contesto con -1 a: "+data.getOrigen());
                 }
                    try {
                                            
                        try (DatagramSocket cliente = new DatagramSocket()) {
                            data2 = new Datos(data.getBusqueda(),this.id);
                            data2.setDestino(data.getDestino());
                            System.out.println("el nodo que solicita :"+data.getDestino());
                           // mjs=mjs+"\n"+"el nodo que solicita :"+data.getDestino();
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            ObjectOutputStream oos = new ObjectOutputStream(baos);
                            oos.writeObject(data2);
                            oos.flush();
                            
                            byte [] b = baos.toByteArray();
                            DatagramPacket paquete2 = new DatagramPacket(b,b.length,InetAddress.getLocalHost(),siguiente-1000);
                            cliente.send(paquete2);
                            
                            DatagramPacket paquete3 = new DatagramPacket (new byte[512],512);
                            //ESPERO RESPUESTA DEL SIGUIENTE NODO
                            
                            cliente.receive(paquete3);
                           
                            ByteArrayInputStream baosIn2 = new ByteArrayInputStream(paquete3.getData());
                            ObjectInputStream ois2 = new ObjectInputStream(baosIn2);
                            data3 = (Datos)ois2.readObject();
                            if(!data.getDestino().equals(Integer.toString(siguiente))){
                                if(data3.getBusqueda().equals("-1"))
                                log.setText(log.getText()+"\nel nodo: "+siguiente+" contesto :"+data3.getBusqueda()+" no se encontro el archivo");
                                else
                                 log.setText(log.getText()+"\nel nodo: "+ siguiente+" contesto :"+data3.getBusqueda()+ " se descargara desde el puerto : "+data3.getPuertoflujo());
                            }
                            ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
                            ObjectOutputStream oos2 = new ObjectOutputStream(baos2);
                            oos2.writeObject(data3);
                            oos2.flush();
                            byte [] b2 = baos2.toByteArray();
                            DatagramPacket respuesta = new DatagramPacket(b2,b2.length,paquete.getAddress(),paquete.getPort());
                            cliente.send(respuesta);
                            if(!data.getDestino().equals(Integer.toString(siguiente))){
                                log.setText(log.getText()+"\nle contesto a: "+data.getOrigen());
                            }
                            cliente.close();
                        }
                    } catch (Exception e) {e.printStackTrace();
                    }
                    
                }
                
                else{//es el nodo destino
                    try {
                        try (DatagramSocket cliente = new DatagramSocket()) {
                            Datos data4 = new Datos();
                            data4.setBusqueda("-1");
                            ByteArrayOutputStream baos2 = new ByteArrayOutputStream();
                            ObjectOutputStream oos2 = new ObjectOutputStream(baos2);
                            oos2.writeObject(data4);
                            oos2.flush();
                            byte [] b2 = baos2.toByteArray();
                            DatagramPacket respuesta = new DatagramPacket(b2,b2.length,paquete.getAddress(),paquete.getPort());
                            cliente.send(respuesta);
                            // log.setText(mjs);
                            cliente.close();
                        }
                        
                    } catch (Exception e) {e.printStackTrace();
                    }
                }
               
            }
        } catch (Exception e) {e.printStackTrace();
        }
                
    }
    
    private boolean buscarArchivo (String nombre){
        ///home/alexis/NetBeansProjects/examen2_anillo/archivos
        File dir = new File("archivos/"+id);
        File [] list = dir.listFiles();
        System.out.println("archivos en el nodo: "+id);
        
        mjs= log.getText()+"\narchivos en el nodo:"+id;
       log.setText(mjs);
        if(list!=null)
            for(File fil : list){
                log.setText(log.getText()+"\n"+fil.getName());
                if(nombre.equals(fil.getName()))
                    return true;
                System.out.println(fil.getName());
                
                
            }
    
        //dir.deleteOnExit();
       
        return false;
    }
    
    private void ServidorFlujo(int id,String NameFIle){
        int pto = id+100;
        String carpeta = Integer.toString(id);
        try {
            try (ServerSocket servidor = new ServerSocket(pto)) {
                Socket s = servidor.accept();
                int flujo = id+100;
                String f = Integer.toString(flujo);
                File archivo = new File("archivos/"+carpeta+"/"+NameFIle);
                String arch_paht = archivo.getAbsolutePath();
                String name = archivo.getName();
                Long tam = archivo.length();
                DataOutputStream dos = new DataOutputStream(s.getOutputStream());
                DataInputStream dis = new DataInputStream(new FileInputStream(arch_paht));
                dos.writeUTF(name);
                dos.flush();
                dos.writeLong(tam);                
                dos.flush();
                byte [] elbyte = new byte [1024];
                long enviado = 0;
                int porcentaje,n;
                while (enviado < tam){
                    n=dis.read(elbyte);
                    dos.write(elbyte, 0, n);
                    dos.flush();
                    enviado = enviado +n;
                    porcentaje = (int)(enviado*100/tam);
                    System.out.println("enviando"+porcentaje+"%");
                    mjs= log.getText()+"\nenviando"+porcentaje+"%";
                    log.setText(mjs);
                    
                }
                dos.close();
                dis.close();
                s.close();
            }
                
        } catch (Exception e) {e.printStackTrace();
        }
       
    }
}
