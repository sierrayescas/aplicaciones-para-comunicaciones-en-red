import java.net.*;
import java.io.*;
import javax.swing.JFileChooser;

public class CArchTCPB {
    public static void main(String[] Args){
        try{
            BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
            String host="127.0.0.1";
            int pto=7000;
            Socket cl=new Socket(host, pto);
            System.out.printf("Cliente Conectado");
            DataInputStream dis = null;
            JFileChooser jf=new JFileChooser();
            jf.setMultiSelectionEnabled(true);
            int r=jf.showOpenDialog(null);
            DataOutputStream dos=new DataOutputStream(cl.getOutputStream());
            if(r==JFileChooser.APPROVE_OPTION){
                File[] f=jf.getSelectedFiles();
                int num=f.length;
                System.out.printf("\nNumero de Archivos:" +num+"\n");
                dos.writeInt(num);  
                for (int i=0; i<f.length; i++) {
                    System.out.printf("\nArchivo Numero "+(i+1)+"\n");
                    System.out.printf(f[i].getName() + "," +f[i].getAbsolutePath()+ ","+f[i].length()+"\n");
                    String archivo=f[i].getAbsolutePath();
                    String nombre=f[i].getName();
                    long tam=f[i].length();
                    dis=new DataInputStream(new FileInputStream(archivo));
                    dos.flush();
                    dos.writeUTF(nombre);
                    dos.flush();
                    dos.writeLong(tam);
                    dos.flush();
                    //Seccion para el envio del archivo
                    byte[] b=new byte[1024];
                    long enviados=0;
                    int porcentaje, n;
                    System.out.print("Archivo "+ i);
                    while(enviados<tam){
                        n=dis.read(b);
                        dos.write(b,0, Math.min(b.length, (int)tam));
                        dos.flush();
                        enviados=enviados+n;
                        porcentaje=(int)(enviados*100/tam);
                        System.out.print("Enviado: "+porcentaje+"%\r");
                    }//while
                    System.out.print("\nArchivo Enviado\n\n");
                
                }
                dis.close();
                dos.close();
                cl.close();
            }//if
        }catch(Exception e){
            e.printStackTrace();
        }//try-catch
    }//main
}//class
