import java.net.*;
import java.io.*;

public class SArchTCPB {
    public static void main(String[] Args){
        try{
            //Creamos Socket
            ServerSocket s = new ServerSocket(7000);
            System.out.printf("Servidor Iniciado...\n");
            //Inciamos el ciclo infinito
            for(;;){
                //Esperamos Conexion
                Socket cl=s.accept();
                System.out.println("Conexion establecida desde" +cl.getInetAddress()+":"+cl.getPort());
                DataInputStream dis = new DataInputStream(cl.getInputStream());
                int numero=dis.readInt();
                System.out.println("Archivos a recibir: " +numero);
                byte[] b= new byte[1024];
                int ta=0;
                for(int i=0; i<numero; i++){
                    String nombre=dis.readUTF();
                    long tam=dis.readLong();
                    System.out.println("\nRecibiemos el Archivo Numero "+(i+1)+": "+nombre);
                    DataOutputStream dos=new DataOutputStream(new FileOutputStream(nombre));
                    //Seccion para recibir el archivo
                    long recibidos=0;
                    int n, porcentaje;
                    while(recibidos<tam){
                        n=dis.read(b, 0, Math.min(b.length, (int)tam));
                        ta=ta+n;
                        dos.write(b,0,n);
                        dos.flush();
                        recibidos=recibidos+n;
                        porcentaje=(int)(recibidos*100/tam);
                        System.out.print("Recibido: "+porcentaje+"%\r");
                    }//while
                    System.out.print("\nArchivo Recibido.\n");
                    dos.close();
                }
                
                dis.close();
                cl.close(); 
            }//for
        }catch(Exception e){
            e.printStackTrace();
        }//try-Catch
    }//main
}//class
