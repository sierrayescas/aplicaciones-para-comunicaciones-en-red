/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datagrama;

import java.net.*;
import java.io.*;

/**
 *
 * @author esierray1500
 */
public class CEcoUDPB {

    public static void main(String[] args) {
        try {
            DatagramSocket cl = new DatagramSocket();
            System.out.println("Clinte iniciado, esccriba un mensaje");
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String mensaje = br.readLine();
            byte[] b = mensaje.getBytes();
            String dst = "127.0.0.1";
            int pto = 2000;
            int offS = 0;
            int lg = 20;
            while (offS < b.length) {
                if (b.length < offS + 20) {
                    lg = b.length - offS;
                }
                DatagramPacket p = new DatagramPacket(b, offS, lg, InetAddress.getByName(dst), pto);
                offS += 20;
                cl.send(p);//enviar
            }
            System.out.println("\nSe enviaron todos los datos");
            //recivir
            DatagramPacket pr = new DatagramPacket(new byte[20], 20);
            String msj = "";
            offS = 0;
            cl.receive(pr);
            for (offS = 0; pr.getLength() > 0; offS += 20) {
                System.out.println("Datagrama recibido desde: " + pr.getAddress() + ":" + pr.getPort());
                msj += new String(pr.getData(), 0, pr.getLength());
                System.out.println("Con el mensaje: " + msj);
                cl.receive(pr);//bloqueo
            }

            //cl.close();
        } catch (Exception e) {
            e.printStackTrace();
        }//hacerlo de comunicacion bidireccional
    }

}
